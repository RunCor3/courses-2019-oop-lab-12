﻿using System;
using System.Collections.Generic;

namespace IndexerGenerics
{
    public class Map2D<TKey1, TKey2, TValue> : IMap2D<TKey1, TKey2, TValue>
    {
        private IDictionary<Tuple<TKey1, TKey2>, TValue> values;
        /*
         * Suggerimento: come struttura dati interna per la memorizzazione dei valori della mappa
         * si suggerisce di utilizzare un Dictionary generico contenente come chiave una tupla di due
         * valori (le chiavi della mappa) e come valore il valore della mappa.
         * 
         * Esempio:
         * private IDictionary<Tuple<TKey1,TKey2>,TValue> values;
         */

        public Map2D()
        {
            values = new Dictionary<Tuple<TKey1, TKey2>, TValue>();
        }

        public void Fill(IEnumerable<TKey1> keys1, IEnumerable<TKey2> keys2, Func<TKey1, TKey2, TValue> generator)
        {
            foreach(var key1 in keys1)
            {
               foreach(var key2 in keys2)
                {
                    var KeyTuple = Tuple.Create(key1, key2);
                    values.Add(KeyTuple, generator(key1, key2));
                }
            }
            /*
             * Nota: si verifichi il funzionamento del metodo Invoke() su oggetti di classe Func
             */

        }

        public bool Equals(IMap2D<TKey1, TKey2, TValue> other)
        {
            foreach(var entry in values)
            {
                var key1 = entry.Key.Item1;
                var key2 = entry.Key.Item2;
                if(other[key1,key2].Equals(entry.Value))
                {

                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        public TValue this[TKey1 key1, TKey2 key2]
        {
            /*
             * Suggerimento: si noti che sulla classe Dictionary è definito un indicizzatore per i valori di chiave
             */

            get
            {
                var KeyTuple = Tuple.Create(key1, key2);
                return values[KeyTuple];        
            }

            set
            {
                var KeyTuple = Tuple.Create(key1, key2);
                values[KeyTuple] = value;
            }
        }

        public IList<Tuple<TKey2, TValue>> GetRow(TKey1 key1)
        {
            List<Tuple<TKey2, TValue>> RowByKey1 = new List<Tuple<TKey2, TValue>>();
            foreach(var entry in values)
            {
                if (entry.Key.Item1.Equals(key1))
                {
                    var Key2ValTuple = Tuple.Create(entry.Key.Item2, entry.Value);
                    RowByKey1.Add(Key2ValTuple);
                }
            }
            return RowByKey1;
        }

        public IList<Tuple<TKey1, TValue>> GetColumn(TKey2 key2)
        {
            List<Tuple<TKey1, TValue>> ColumnByKey2 = new List<Tuple<TKey1, TValue>>();

            foreach(var entry in values)
            {
                if (entry.Key.Item2.Equals(key2))
                {
                    var Key1ValTuple = Tuple.Create(entry.Key.Item1, entry.Value);
                    ColumnByKey2.Add(Key1ValTuple);
                }
            }
            return ColumnByKey2;

        }

        public IList<Tuple<TKey1, TKey2, TValue>> GetElements()
        {
            List<Tuple<TKey1, TKey2, TValue>> list = new List<Tuple<TKey1, TKey2, TValue>>();

            foreach(var entry in values)
            {
                var dataTuple = Tuple.Create(entry.Key.Item1, entry.Key.Item2, entry.Value);
                list.Add(dataTuple);
            }

            return list;
        }

        public int NumberOfElements
        {
            get
            {
                int CountValues = 0;
                foreach(var entry in values)
                {
                    CountValues++;
                }
                return CountValues;
            }
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
