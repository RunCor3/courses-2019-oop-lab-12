﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace DelegatesAndEvents {
    public class ObservableList<TItem> : IObservableList<TItem>
    {
        //private ObservableList<TItem> this = new ObservableList<TItem>();

        
        private IList<TItem> lista = new List<TItem>();

        public IEnumerator<TItem> GetEnumerator()
        {
            return lista.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(TItem item)
        {
            lista.Add(item);
        }

        public void Clear()
        {
            lista.Clear();
        }

        public bool Contains(TItem item)
        {
            return lista.Contains(item);
        }

        public void CopyTo(TItem[] array, int arrayIndex)
        {
            lista.CopyTo(array, arrayIndex);
        }

        public bool Remove(TItem item)
        {
            return lista.Remove(item);
        }

        public int Count
        {
            get
            {
                return lista.Count;
            }
            
        }

        public bool IsReadOnly
        {
            get
            {
                return lista.IsReadOnly;

            }
        }
        public int IndexOf(TItem item)
        {
            return IndexOf(item);
        }

        public void Insert(int index, TItem item)
        {
            lista.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            lista.RemoveAt(index);
        }

        public TItem this[int index]
        {
            get { return lista[index]; }
            set { lista[index] = value; }
        }

        public event ListChangeCallback<TItem> ElementInserted;
        public event ListChangeCallback<TItem> ElementRemoved;
        public event ListElementChangeCallback<TItem> ElementChanged;

        public override string ToString()
        {
            return base.ToString();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

}